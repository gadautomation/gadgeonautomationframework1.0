# =====================================================================================
#    Filename   :  ElementLocators_Android.py
#
#    Description:  This file contains all the user defined values for element locators in Android
#
#    Version    :  1.0
#    Created    :  14 Dec 2017
#    Compiler   :  python
#    Author     :  Geon
#    Company    :  Gadgeon, Kochi
#
#    Revision History:
#
# =====================================================================================

######################################################################
# Page Name : Genaral 
# comment : General usage Elements in  App
######################################################################
message_box = u"id=android:id/message"
message_box_ok = u"id=android:id/button1"

######################################################################
# Page Name : Login
# comment : Elements in Login Page
######################################################################
login_username = u"id=com.example.nison.freelance:id/textView"
login_password = u"id=com.example.nison.freelance:id/textView1"
login_button = u"id=com.example.nison.freelance:id/button_login"