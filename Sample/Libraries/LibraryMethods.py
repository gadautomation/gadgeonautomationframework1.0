# ===================================================================================== 
#    Filename   :  LibraryMethods.py
#
#    Description:  This library file contains all the user defined fucntions used for 
#               :  automation framework.
#
#    Version    :  1.0
#    Created    :  14 Dec 2017
#    Compiler   :  python
#    Author     :  Geon
#    Company    :  Gadgeon, Kochi
#
#    Revision History:
#
# =====================================================================================

#======= Settings =========

######################################################################
# Func Name : LibFunction
# Arg1 : [Argument Name]
# returns : Return Value
######################################################################
def LibFunction(arg):
    print arg
    val = 'Hello'
    return val