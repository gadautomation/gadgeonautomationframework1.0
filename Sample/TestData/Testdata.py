# =====================================================================================
#    Filename   :  Testdata.py
#
#    Description:  This file contains all the user defined values
#
#    Version    :  1.0
#    Created    :  14 Dec 2017
#    Compiler   :  python
#    Author     :  Geon
#    Company    :  Gadgeon, Kochi
#
#    Revision History:
#
# =====================================================================================

######################################################################
# Data Set Name : USER DATA
# Useage        : For Login
# comment       : Variables used for Login
#
######################################################################
Username = u"nison"
Userpassword = u"nison"
Invalid_Username = u"invalied"
Invalid_Userpassword = u"invalied"

######################################################################
# Data Set Name : Home
# Useage        : For Validating Home page
# comment       : Variables used for check Home page
######################################################################
txt_home_page = u"welcome to home page"
