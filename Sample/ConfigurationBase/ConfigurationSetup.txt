# ############################################################################
#    Filename   :  Configurationsetup.txt
#
#    Description:  This txt file contains all the user defined methods which are generic in nature.
#
#    Version    :  1.0
#    Created    :  14 Dec 2017
#    Compiler   :  python
#    Author     :  Geon
#    Company    :  Gadgeon, Kochi
#
#    Revision History:
#
################################################################################

*** Settings ***

Library     AppiumLibrary  10
Variables    Framework_Properties.py

Resource    ../ResourceFiles/Login_ResourceFile.txt

*** Keywords ***

###################################################################
# KeyWord Name : Open_App
# Usage        : for Open the App
###################################################################
Open_App
    # For android version 7 less
    ${App1} =    Open Application	${Appium_server}	platformName=${Platform_name}	platformVersion=${Platform_version}	deviceName=${Device_name}  app=${CURDIR}/../Resources/${Application_APK_name}    appActivity=${Activity_name}    appPackage=${Package_name}
	
    # For android version 7 and More
    #${App1} =    Open Application	${Appium_server}	platformName=${Platform_name}	platformVersion=${Platform_version}	deviceName=${Device_name}  app=${CURDIR}/../Resources/${Application_APK_name}    appActivity=${Activity_name}    appPackage=${Package_name}    automationName=uiautomator2
    
    [Return]   ${App1}

###################################################################
# KeyWord Name : Open_App_new
# Usage        : for Open the App
###################################################################
Open_App_new
    # For android version 7 less
    ${App2} =    Open Application	${Appium_server_new}	platformName=${Platform_name}	platformVersion=${Platform_version_new}	deviceName=${Device_name_new}  app=${CURDIR}/../Resources/${Application_APK_name}    appActivity=${Activity_name}    appPackage=${Package_name}
    
    # For android version 7 and More
    #${App2} =    Open Application	${Appium_server_new}	platformName=${Platform_name}	platformVersion=${Platform_version_new}	deviceName=${Device_name_new}  app=${CURDIR}/../Resources/${Application_APK_name}    appActivity=${Activity_name}    appPackage=${Package_name}    automationName=uiautomator2
    [Return]    ${App2}

###################################################################
# KeyWord Name : Open_App_noreset
# Usage        : for Open the App without rest the values
###################################################################
Open_App_noreset
    # For android version 7 less
    ${App1} =    Open Application	${Appium_server}	platformName=${Platform_name}	platformVersion=${Platform_version}	deviceName=${Device_name}  app=${CURDIR}/../Resources/${Application_APK_name}    appActivity=${Activity_name}    appPackage=${Package_name}    fullReset=false    noReset=true

    # For android version 7 and More
    #${App1} =    Open Application	${Appium_server}	platformName=${Platform_name}	platformVersion=${Platform_version}	deviceName=${Device_name}  app=${CURDIR}/../Resources/${Application_APK_name}    appActivity=${Activity_name}    appPackage=${Package_name}    automationName=uiautomator2    fullReset=false    noReset=true
    [Return]   ${App1}

###################################################################
# KeyWord Name : Open_App_new_noreset
# Usage        : for Open the App without rest the values
###################################################################
Open_App_new_noreset
    # For android version 7 less
    ${App1} =    Open Application	${Appium_server}	platformName=${Platform_name}	platformVersion=${Platform_version}	deviceName=${Device_name}  app=${CURDIR}/../Resources/${Application_APK_name}    appActivity=${Activity_name}    appPackage=${Package_name}    automationName=uiautomator2    fullReset=false    noReset=true
    
    # For android version 7 and More
    #${App1} =    Open Application	${Appium_server}	platformName=${Platform_name}	platformVersion=${Platform_version}	deviceName=${Device_name}  app=${CURDIR}/../Resources/${Application_APK_name}    appActivity=${Activity_name}    appPackage=${Package_name}    automationName=uiautomator2    fullReset=false    noReset=true
    [Return]    ${App2}

###################################################################
# KeyWord Name : Teardown_App1
# Usage        : Function for Teardown the test case 
###################################################################
Teardown_App1
	Log To Console    \nteardownthe function ${TEST STATUS} ${SUITE NAME}
    Run Keyword if    '${TEST STATUS}' == 'FAIL'    Restart_App
	Run Keyword if    '${SUITE NAME}' == 'MyMedia'    Teardown_move    ${app_icon}
	...    ELSE IF    '${SUITE NAME}' == 'MyChannels'    Teardown_move    ${app_icon}

###################################################################
# KeyWord Name : Teardown_App
# Usage        : Function for Teardown the test Suite with login  
###################################################################
Teardown_App
    Run Keyword if    '${TEST STATUS}' == 'FAIL'    Restart_App

###################################################################
# KeyWord Name : Teardown_App_Login
# Usage        : Function for Teardown the test Suite without login
###################################################################
Teardown_App_Login
    Run Keyword if    '${TEST STATUS}' == 'FAIL'    Restart_App_with_login

###################################################################
# KeyWord Name : Restart_App_with_login
# Usage        : Function for close the app then again restart the app
###################################################################
Restart_App_with_login
    Close Application
    Open_App_noreset
    Verify_permissions1
    Login_signup_page
    Login_username_field_check

###################################################################
# KeyWord Name : Restart_App
# Usage        : Function for close the app then open the app without reset the app
###################################################################
Restart_App
    Close Application
    Open_App_noreset

###################################################################
# KeyWord Name : Teardown_move
# Usage        : Function for move back to the given location
###################################################################
Teardown_move
	[Arguments]    ${Location}
    : FOR     ${INDEX}    IN RANGE     10
         \      Sleep   1
         \      ${present}=  BuiltIn.Run Keyword And Return Status    AppiumLibrary.Page should contain Element    ${Location}
         \      Run Keyword If    ${present}
         \      ...     Exit For Loop If	${present}
         \      ...    ELSE
         \      ...      AppiumLibrary.Go Back
