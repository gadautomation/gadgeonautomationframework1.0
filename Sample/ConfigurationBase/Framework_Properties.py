# =====================================================================================
#    Filename   :  Framework_Properties.py
#
#    Description:  This file contains all the user defined values used for frame work.
#
#    Version    :  1.0
#    Created    :  14 Dec 2017
#    Compiler   :  python
#    Author     :  Geon
#    Company    :  Gadgeon, Kochi
#
#    Revision History:
#
# =====================================================================================

######################################################################
# Useage        : Automation system setup
# comment       : Variables used for Open/stating the automation system
#
######################################################################
Selenium_server = ""                            #Selenium server path

Browser_name = "firefox"                        #Mention chrome or firefox

Appium_server = "http://localhost:4723/wd/hub"  #Appim server path

Platform_type = "Mobile"                        #specify mobile or web application

Platform_version = "7.0"                        #Android version of the Device

Platform_name = "Android"                       #Specify whether Android or iOS

Device_name = "ZY223WNNM6"                        #Name of the device

Application_APK_name = "app-debug.apk"          #APK name in "Resources" location

Package_name = "com.example.nison.freelance"    #Package name of APK

Activity_name = "com.example.nison.freelance.MainActivity" #Activity name of APK