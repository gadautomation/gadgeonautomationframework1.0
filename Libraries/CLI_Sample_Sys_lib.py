# =====================================================================================
#    Filename   :  CLI_Sample_Sys_lib.py
#
#    Description:  This lib file contains all the user defined fucntions used for
#               :  CLI Test Cases for Point Master
#
#    Version    :  {version}
#    Created    :  {Date of create}
#    Compiler   :  python
#    Author     :  {Author Name}
#    Company    :  Gadgeon, Kochi 
#
#    Revision History:
#
# =====================================================================================

#!/usr/bin/env python
from CLI_Sample_Sys_ParamFile import *
import CLI_Sample_Sys_ParamFile
import os
import subprocess
import pexpect
import time
import threading
import sys

global ip
ip= ConfigFrameWork.DUTDETAILS[0][0]  # fetches ip of test device ip=192.168.2.95

#Used to fetch Command from CLI_Sample_Sys_ParamFile.py by index
def fetch_command(listindex):
	result ="unknown"
	listindex = int(listindex)
	result = CLI_Sample_Sys_ParamFile.listofnames[listindex]
	return result

# Used to login in SSH
def wscliLogin(ip,user,password):
	cmd ='ssh '+user+'@'+ip
	global child
	child = pexpect.spawn (cmd)
	child.timeout = 30
   	print ("Connecting to",ip)
	child.expect (ip)
	if user in  child.before:
		child.sendline(password)
	elif "authenticity" in  child.before:
	 	child.send('yes\r')
	 	child.expect ("password:")
	 	child.send(password)
	time.sleep(5)

# child.send will issue a command into the terminal
# child.expect will wait till the timeout specified for the response specified
# and will result in error if the expected value is not received

# After the expected value is received this fuction issues command
def Shell_Command(command,expectedValue):
	child.expect (expectedValue)
	child.sendline(command)
	child.timeout = 30
	print (child.before)
	return child.before

# Inside CLI this function waits till an expected Text response in received
def Kid_Expect(kid):
	child.timeout = 30
	try:
		child.expect (kid)
		print ("Found >> ",kid)
	except:
		print ("Expected Message Not Found")
		print ("child.before",child.before)
		print ("child.after",child.after)
		raise AssertionError("Expected Response Not Found")

# Used to SSH into test device.
def Cli_Parser(clilist):
	global ip
	print ("IP:",ip)
	mylist=[]
	mylist=eval(clilist) # loads the value in Paramfile into a list
	username ='wscli'
	paswd ='Cli@Dntl#40'
	wscliLogin(ip,username,paswd)
	for i in mylist:
		eval(i)			# eval converts the string recived into a python function call



