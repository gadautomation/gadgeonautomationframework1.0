# =====================================================================================
#    Filename   :  CLI_Sample_Sys_ParamFile.py
#
#    Description:  This param file contains all the user defined values used for
#               :  Webshield frame work.
#
#    Version    :  {version}
#    Created    :  {Date of create}
#    Compiler   :  python
#    Author     :  {Author Name}
#    Company    :  Gadgeon, Kochi 
#
#    Revision History:
#
# =====================================================================================

listofnames=['systesting']

##############################################################################
# List containing functions defined to issue commands and check response
##############################################################################


# if the function Shell_Command receives value dantel then  it will issued the command pml.

systesting=[
	"Shell_Command('pml','dantel')",
	"Shell_Command('sys','Pointmaster> ')",


	"Kid_Expect('MAT    1 . . . . . . . . . . . . . . . .    1- 16')",
	"Kid_Expect('MAT    2 . . . . . . . . . . . . . . . .   17- 32')",
	"Kid_Expect('MAT    3 . . . . . . . . . . . . . . . .   33- 48')",
	"Kid_Expect('MAT    4 . . . . . . . . . . . . . . . .   49- 64')",
	"Kid_Expect('MAT    5 . . . . . . . . . . . . . . . .   65- 80')",
	"Kid_Expect('MAT    6 . . . . . . . . . . . . . . . .   81- 96')",
	"Kid_Expect('MAT    7 . . . . . . . . . . . . . . . .   97-112')",
	"Kid_Expect('MAT    8 . . . . . . . . . . . . . . . .  113-128')",
	"Kid_Expect('MAT    9 . . . . . . . . . . . . . . . .  129-144')",
	"Kid_Expect('MAT   10 . . . . . . . . . . . . . . . .  145-160')",
	"Kid_Expect('MAT   11 . . . . . . . . . . . . . . . .  161-176')",

	"Shell_Command('exit','Pointmaster> ')",
	"Shell_Command('exit','dantel')",
	]


