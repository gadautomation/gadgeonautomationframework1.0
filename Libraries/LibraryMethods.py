# ===================================================================================== 
#    Filename   :  LibraryMethods.py
#
#    Description:  This library file contains all the user defined fucntions used for 
#               :  automation framework.
#
#    Version    :  {version}
#    Created    :  {Date of create}
#    Compiler   :  python
#    Author     :  {Author Name}
#    Company    :  Gadgeon, Kochi 
#
#    Revision History:
#
# =====================================================================================

#======= Settings =========


######################################################################
# Func Name : LibFunction
# Arg1 : [Argument Name]
#
# returns : Return Vale
######################################################################
def LibFunction(username):
    val = 'Hello'
    return val
