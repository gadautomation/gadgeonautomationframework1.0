# =====================================================================================
#    Filename   :  ElementLocators_Web.py
#
#    Description:  This file contains all the user defined values for element locators in Web
#
#    Version    :  {version}
#    Created    :  {Date of create}
#    Compiler   :  python
#    Author     :  {Author Name}
#    Company    :  Gadgeon, Kochi 
#
#    Revision History:
#
# =====================================================================================

######################################################################
# Page Name : {Name of the App Page}
# comment :  [Comment]
#
######################################################################
#locationName = "id=com.android.packageinstaller:id/locationName"
