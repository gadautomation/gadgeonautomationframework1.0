# =====================================================================================
#    Filename   :  ElementLocators_Android.py
#
#    Description:  This file contains all the user defined values for element locators in Android
#
#    Version    :  {version}
#    Created    :  {Date of create}
#    Compiler   :  python
#    Author     :  {Author Name}
#    Company    :  Gadgeon, Kochi 
#
#    Revision History:
#
# =====================================================================================

######################################################################
# Page Name : {App Page Name}
# comment : [Command]
#
######################################################################
Appicon = "id=com.android.packageinstaller:id/AppIcon"

