# =====================================================================================
#    Filename   :  Framework_Properties.py
#
#    Description:  This file contains all the user defined values used for
#               :  frame work.
#
#    Version    :  {version}
#    Created    :  {Date of create}
#    Compiler   :  python
#    Author     :  {Author Name}
#    Company    :  Gadgeon, Kochi 
#
#    Revision History:
#
# =====================================================================================


##########################Framework Properties###################################################

Selenium_server = ""                                #Selenium server path

Browser_name = " "                            #Mention chrome or firefox

Url = " "

Appium_server = "http://localhost:4723/wd/hub"  #Appim server path

Platform_type = "Mobile"                    #Specify mobile or web application

Platform_version = "6.0"                    #Specify platform version of the device

Platform_name = "Android"                   #Specify whether Android or iOS

Device_name = " "                    #Device UDID

Application_APK_name = "app-debug.apk"      #Application APK/IPA Path

Package_name = " "          #Application package name

Activity_name = " "#Application activity name

appActivity = " "

appWaitActivity = " "

#####################Shell Automation##############################################################

TestDevice_IP = " " #IP of the device under test

ssh_username = " " #username to ssh

ssh_password= " " #Password to ssh


######################Minicom Automation#########################################################

Minicom_log_filepath = " "      #Minicom Output log filepath

Minicom_log_filename = " "      #Minicom Output log filename

####################TestRail Integration########################################################

TestRailUrl = " "     #TestRail URL

TestRailUser = " "    #Provide username to login to TestRail

TestRailPassword = " "          # Provide password to login to TestRail

ProjectName = " "                  #Provide Project name

PlanName = " "                   #Provide Plan name

CONFIG1 = " "                    #Provide Configuration (5.0, Chrome)

##################################################################################################
