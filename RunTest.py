# =====================================================================================
#    Filename   : RunTest.py
#
#    Description: This Python lib is used to run the cases defined in ConfigFramework.py
#
#    Version    :  {version}
#    Created    :  {Date of create}
#    Compiler   :  python
#    Author     :  {Author Name}
#    Company    :  Gadgeon, Kochi 
#
#    Revision History:
#
# =====================================================================================

import os
import time
import datetime
import subprocess

#######################################################################
# Func Name : ExecuteTestCases
# returns   : TestCase Name with FilePath
########################################################################
def ExecuteTestCases():
    cmd = "sudo pybot --outputdir Results/"+DateTime+"/TestSuite --output output.xml Tests/TestSuite.txt"
    os.system(cmd)
    
##########################################################################
# Func Name : CombineLogs
# returns   : This function combines the induvidal logs into a single log
#########################################################################
def CombineLogs():
    rebot_files = ""
    cmd = ""
    rebot_files = subprocess.check_output(
        "find ./Results/"+DateTime+" -iname *.xml | xargs ls -tr | tr '\n' ' '", shell=True)
    cmd = "sudo rebot --name CATCHAutomationReport --outputdir Results/"+DateTime+"  -o output.xml --removekeywords passed --tagstatexclude FN* --tagstatexclude SH* " + \
        rebot_files
    os.system(cmd)

if __name__== "__main__":
    DateTime = datetime.datetime.now().strftime("%Y_%h_%d_%H_%M")
    print DateTime
    ExecuteTestCases()
    CombineLogs()
