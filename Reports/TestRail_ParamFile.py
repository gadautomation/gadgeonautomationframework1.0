# =====================================================================================
#    Filename   :  RFW_ParamFile.py
#
#    Description:  This para file contains all the user defined values used for 
#               :  {Project Name} frame work.
#
#    Version    :  {version}
#    Created    :  {Date of create}
#    Compiler   :  python
#    Author     :  {Author Name}
#    Company    :  Gadgeon, Kochi
#
#    Revision History:
#
# =====================================================================================



#########################################################################################
# Array Name : Basic_Parameter_Details
# Usage : For Basic Test parameter configuration
#
# Value0 : Wifi SSID 
# Value1 : Wifi Channel
# Value2 : Wifi Password
# Value3 : Wifi Mac Address Patch
# Value4 : Wifi Mac Address Adapter
# Value5 : Sleep Time for Adapter and Patch Firmware Flash Check TC1
# Value6 : Threshold value(percentage) for reach utility counter check for Adapter and Patch Firmware Flash Check TC1
# Value7 : Time Interval for between couters check TC6
# Value8 : Length of Packets In Bytes TC6
# Value9 : Threshold value(percentage) for reach utility counter check for Security In Non LWIP Mode Check TC8
# Value10: Sleep Time for Standard Access Point and Patch Firmware Flash Check TC3
# Value11: Threshold value(percentage) for reach utility counter check for Patch Standard AP Check TC3
# Value12: Version for binary download
# Value13: Test period for TestCase 3
# Value14: Time interval for polling counters

#########################################################################################

#from RunTest import *
TestRailUrl = ""
TestRailUser = ""
TestRailPassword = ''
ProjectName = ""
PlanName = ""
CONFIG1 = ""
#print CONFIG1

