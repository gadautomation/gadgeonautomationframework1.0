# =====================================================================================
#    Filename   :  Testdata.py
#
#    Description:  This file contains all the user defined values for element locators
#
#    Version    :  {version}
#    Created    :  {Date of create}
#    Compiler   :  python
#    Author     :  {Author Name}
#    Company    :  Gadgeon, Kochi
#
#    Revision History:
#
# =====================================================================================

######################################################################
# Data Set Name : { Name of Data Set }
# Useage        : { Usage of Data Set}
# comment       : [comment of Data Set]
#
######################################################################
User_name = u"user"
User_password = u"password"